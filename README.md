# Custom Autocomplete Labels

This Drupal 8 module is primarily used as an example of how to customize autocomplete labels.

# Structure

The magic happens in the [EntityAutocompleteMatcher.php](src/EntityAutocompleteMatcher.php). It will change autocomplete labels to follow these patterns:

* Taxonomy Format: Parent Entity Label → Entity Label
* Product Format: Product Label (Product Type) - Unpublished and Store Unavailability Indicator
* All Other Entities Format: Entity Label (Entity ID) - Unpublished Indicator

The `CustomAutocompleteLabelsServiceProvider` class is only needed to override the core matcher service with our own.

# Usage

It is encouraged to NOT use thtis module directly. Instead, copy this into your project's `modules/custom` directory and adjust as your use case needs. (Note: be sure to remove the .git directory in this repo after you copy it. This avoids the module being committed as a git submodule.)

Alternatively, you can use this module directly by adding this to your composer.json in the "repositories" section:

```
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:bounteous/drupal-custom-autocomplete-labels.git"
        }
```

Then, require the module with:

```
composer require bounteous/custom_autocomplete_labels
```

Enable the module and all your autocomplete labels will be customized according to the patterns explained in [#Structure](#Structure).

# See Also

See also the "[View output is not used for entityreference options](https://www.drupal.org/project/drupal/issues/2174633)" issue. Once it is resolved, instead of writing custom code you can create a view with an entity reference display where you can customize the label. You may decide to still use a customization like this module if you don't want to create a view for every type autocomplete.

# License

[MIT](LICENSE)

