<?php

namespace Drupal\custom_autocomplete_labels;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityAutocompleteMatcher as EntityAutocompleteMatcherBase;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Customize autocomplete labels.
 *
 * @see \Drupal\Core\Entity\EntityAutocompleteMatcher
 */
class EntityAutocompleteMatcher extends EntityAutocompleteMatcherBase {

  /**
   * {@inheritdoc}
   */
  public function getMatches($target_id, $selection_handler, $selection_settings, $string = '') {
    $matches = [];

    if (!isset($string)) {
      return $matches;
    }

    // Get the matches with different limits based on type of referenced entity.
    $handler = $this->selectionManager->getInstance([
      'target_type' => $target_id,
      'handler' => $selection_handler,
      'handler_settings' => $selection_settings,
    ]);
    $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
    if ($target_id == 'taxonomy_term' || $target_id == 'commerce_product') {
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, 25);
    }
    else {
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, 10);
    }

    // Customize the labels used in autocomplete.
    foreach ($entity_labels as $values) {
      foreach ($values as $entity_id => $label) {
        if ($target_id == 'taxonomy_term') {
          $custom_label = $this->getLabelForTerm($entity_id, $target_id, $label);
        }
        elseif ($target_id == 'commerce_product') {
          $custom_label = $this->getLabelForProduct($entity_id, $target_id, $label);
        }
        else {
          $custom_label = $this->getLabel($entity_id, $target_id, $label);
        }

        // Create a sanitized key.
        $key = "$label ($entity_id)";
        $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
        $key = Tags::encode($key);

        $matches[] = ['value' => $key, 'label' => $custom_label];
      }
    }

    return $matches;
  }

  /**
   * Format the label for Terms.
   *
   * Format: Parent Entity Label → Entity Label
   *
   * @param int $entity_id
   *   The entity id.
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $label
   *   The entity's label.
   *
   * @return string
   *   The customized label.
   */
  protected function getLabelForTerm($entity_id, $entity_type_id, $label) {
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $parents = $term_storage->loadAllParents($entity_id);

    $first = array_pop($parents);
    $label = $first->getName();
    foreach (array_reverse($parents) as $term) {
      $label = $label . ' → ' . $term->getName();
    }

    return $label;
  }

  /**
   * Format the label for Products.
   *
   * Format: Product Label (Product Type) - Unpublished and Store Unavailability Indicator
   *
   * @param int $entity_id
   *   The entity id.
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $label
   *   The entity's label.
   *
   * @return string
   *   The customized label.
   */
  protected function getLabelForProduct($entity_id, $entity_type_id, $label) {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    $available_store_ids = $entity->getStoreIds();
    $bundle_label = \Drupal::entityManager()->getBundleInfo('commerce_product')[$entity->bundle()]['label'];
    $all_stores = \Drupal::entityTypeManager()->getStorage('commerce_store')->loadMultiple();

    $label = $entity->label() . ' (' . $bundle_label . ')';

    if ($entity instanceof EntityPublishedInterface && !$entity->isPublished()) {
      $label = $label . ' - Unpublished';
    }
    elseif (count($available_store_ids) == 0) {
      $label = $label . ' - Unavailable in all stores';
    }
    else {
      $unavailable_stores = [];
      foreach ($all_stores as $store) {
        if (!in_array($store->id(), $available_store_ids)) {
          $unavailable_stores[] = $store->label();
        }
      }
      if (count($unavailable_stores)) {
        $label = $label . ' - Unavailable in ' . join($unavailable_stores);
      }
    }

    return $label;
  }

  /**
   * Format the label for any entity.
   *
   * Format: Entity Label (Entity ID) - Unpublished Indicator
   *
   * @param int $entity_id
   *   The entity id.
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $label
   *   The entity's label.
   *
   * @return string
   *   The customized label.
   */
  protected function getLabel($entity_id, $entity_type_id, $label) {
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    $entity = \Drupal::entityManager()->getTranslationFromContext($entity);

    $label = $label . ' (' . $entity_id . ')';

    if ($entity instanceof EntityPublishedInterface && !$entity->isPublished()) {
      $label = $label . ' - Unpublished';
    }

    return $label;
  }

}
