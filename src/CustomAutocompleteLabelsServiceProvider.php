<?php

namespace Drupal\custom_autocomplete_labels;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;

/**
 * Replace the autocomplete matcher with our own service.
 */
class CustomAutocompleteLabelsServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('entity.autocomplete_matcher');
    $definition->setClass('Drupal\custom_autocomplete_labels\EntityAutocompleteMatcher');
  }

}
